/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhojava;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Telecom
 */
public class Criador {
    
    private ArrayList<String> funcionalidades; /** funções que vai ter no DAO **/
    private ArrayList<String> campos; /** atributos da classe que vai ser criada **/
    private String titulo; /** titulo da classse **/
    private String primaryKey; /** atributo que é chave primária da classe **/

    public Criador(ArrayList<String> funcionalidades, ArrayList<String> campos, String titulo, String primaryKey) {
        this.funcionalidades = funcionalidades;
        this.campos = campos;
        this.titulo = titulo;
        this.primaryKey = primaryKey;
    }
    
    public void criadorPastas(String titulo) /** cria as pastas base em c:// **/
    {
        File diretorio = new File("C:\\" + titulo + "\\model");
        diretorio.mkdirs();
        File diretorio2 = new File("C:\\" + titulo + "\\controller");
        diretorio2.mkdirs();
        File diretorio3 = new File("C:\\" + titulo + "\\view");
        diretorio3.mkdirs();
        
    }
    
    public void criadorController(String titulo, ArrayList<String> campos, String primaryKey, ArrayList<String> funcionalidades) throws IOException{
        String tipo = "";
        for (String campo: campos) {
            String[] metacampos = campo.split(" / ");
            if (metacampos[0].equals(primaryKey)) {
                tipo = metacampos[1];
                break;
            }
        }
        String classe = titulo.substring(0, 1).toUpperCase().concat(titulo.substring(1));
        String var = titulo.substring(0, 1).toLowerCase().concat(titulo.substring(1));
        BufferedWriter buffer;
        buffer = new BufferedWriter(new FileWriter(new File("C:\\" + titulo + "\\controller\\Controller"+titulo.substring(0, 1).toUpperCase().concat(titulo.substring(1))+".java")));
        buffer.write("package controller; \n");
        buffer.write("import java.sql.SQLException; \n");
        buffer.write("import model.DAO"+classe+";\n");
        buffer.write("import model."+classe+";\n\n");
        buffer.write("public class Controller"+classe+"{\n\n");
        buffer.write("    private DAO"+classe+" dao;\n\n");
        buffer.write("    public Controller"+classe+"() throws SQLException{\n");
        buffer.write("        this.dao = new DAO"+classe+"();\n");
        buffer.write("}\n\n");
        
        for(String n: funcionalidades){
            if (n == "Salvar") {
                //Salvar
                buffer.write("    public String salvar( ");
                int contcampos = 1;
                for (String campo: campos) {
                    String[] campoSeparado = campo.split(" / ");
                    if(contcampos == 1){
                        buffer.write(campoSeparado[1]+" "+campoSeparado[0]);
                    }else{
                        buffer.write(", "+campoSeparado[1]+" "+campoSeparado[0]);
                    }
                    contcampos++;
                }
                buffer.write(") throws SQLException{\n");
                buffer.write("        "+classe+" "+var+" = new "+classe+"();\n");
                for (String campo: campos) {
                    String[] campoSeparado = campo.split(" / ");
                    String atributo = campoSeparado[0].substring(0, 1).toUpperCase().concat(campoSeparado[0].substring(1));
                    buffer.write("        "+var+".set"+atributo+"("+campoSeparado[0]+");\n");
                }
                buffer.write("        return dao.salvar("+var+")? \" OPERAÇÃO BEM-SUCEDIDA! \" : \" OPERAÇÃO FRACASSOU! \";\n");
                buffer.write("}\n\n");
            }
            
            if(n == "Consultar"){
                //Consultar
                buffer.write("    public "+classe+" consultar("+tipo+" "+primaryKey+") throws SQLException{\n");
                buffer.write("        "+classe+" "+var+" = new "+classe+"();\n");
                buffer.write("        "+var+".set"+primaryKey.substring(0, 1).toUpperCase().concat(primaryKey.substring(1))+"("+primaryKey+");\n");
                buffer.write("        return dao.consultar("+var+");\n");
                buffer.write("}\n\n");
            }
            
            if(n == "Editar"){
                 //Editar
                buffer.write("    public String editar( ");
                int contcampos = 1;
                for (String campo: campos) {
                    String[] campoSeparado = campo.split(" / ");
                    if(contcampos == 1){
                        buffer.write(campoSeparado[1]+" "+campoSeparado[0]);
                    }else{
                        buffer.write(", "+campoSeparado[1]+" "+campoSeparado[0]);
                    }
                    contcampos++;
                }
                buffer.write(") throws SQLException{\n");
                buffer.write("        "+classe+" "+var+" = new "+classe+"();\n");
                for (String campo: campos) {
                    String[] campoSeparado = campo.split(" / ");
                    String atributo = campoSeparado[0].substring(0, 1).toUpperCase().concat(campoSeparado[0].substring(1));
                    buffer.write("        "+var+".set"+atributo+"("+campoSeparado[0]+");\n");
                }
                buffer.write("        return dao.editar("+var+")? \" OPERAÇÃO BEM-SUCEDIDA! \" : \" OPERAÇÃO FRACASSOU! \";\n");
                buffer.write("}\n\n");
            }
            
            if(n == "Remover"){
                 //Remover
                buffer.write("    public String remover("+tipo+" "+primaryKey+") throws SQLException{\n");
                buffer.write("        "+classe+" "+var+" = new "+classe+"();\n");
                buffer.write("        "+var+".set"+primaryKey.substring(0, 1).toUpperCase().concat(primaryKey.substring(1))+"("+primaryKey+");\n");
                buffer.write("        return dao.remover("+var+")? \" OPERAÇÃO BEM-SUCEDIDA! \" : \" OPERAÇÃO FRACASSOU! \";\n");
                buffer.write("}\n\n");


                
            }
        }
        buffer.write("}\n\n");
        buffer.close();  
       
    }
    
    public void criadorConnection(String titulo) throws IOException /** cria o conection na pasta model **/
    {
        BufferedWriter buffer;
        buffer = new BufferedWriter(new FileWriter(new File("C:\\" + titulo + "\\model\\ConnectionDatabase.java")));
        buffer.write("package model;\n" +
            "\n" +
            "import java.sql.Connection; \n" +
            "import java.sql.DriverManager; \n" +
            "import java.sql.SQLException; \n \n " + 
            "public class ConnectionDatabase { \n" +
            "    \n" +
            "    public static Connection getConnection() throws SQLException \n" +
            "    { \n" + 
            "        String url = \"jdbc:mysql://localhost/" + titulo + "\"; \n" +
            "        String user = \"root\"; \n" +
            "        String password = \"root\"; \n" +
            "        return DriverManager.getConnection(url, user, password);" +
            "        \n" +
            "    } \n" +
            "    \n" +
            "}");
            
        buffer.close();
            
    }
    
    public void criadorClasse(ArrayList<String> campos, String titulo) throws IOException /** cria a classe com os gets e sets **/
    {
        BufferedWriter buffer;
        buffer = new BufferedWriter(new FileWriter(new File("C:\\" + titulo + "\\model\\" +  titulo.substring(0,1).toUpperCase().concat(titulo.substring(1)) + ".java")));
        buffer.write("package model;\n" +
            "\n" +
            "public class " + titulo.substring(0,1).toUpperCase().concat(titulo.substring(1)) + " { \n" +
            "\n");
            for(String x : campos)
            {
                String[] campoSeparado = x.split(" / ");
                buffer.write("    private " + campoSeparado[1] + " " + campoSeparado[0] + "; \n");   
            }
            buffer.write("\n    public " + titulo.substring(0,1).toUpperCase().concat(titulo.substring(1)) + "(");
            int contCampos = campos.size();
            for(String x : campos)
            {
                contCampos -= 1;
                String[] campoSeparado = x.split(" / ");
                if(contCampos == 0)
                {
                    buffer.write(campoSeparado[1] + " " + campoSeparado[0]);
                }
                else
                {
                     buffer.write(campoSeparado[1] + " " + campoSeparado[0] + ", ");
                }        
            }
            buffer.write(") { \n\n");
            
            for(String x : campos)
            {
                String[] campoSeparado = x.split(" / ");
                buffer.write("        this." + campoSeparado[0] + " = " + campoSeparado[0] + "; \n");   
            }
            buffer.write("    } \n \n");
            
            buffer.write("\n    public " + titulo.substring(0,1).toUpperCase().concat(titulo.substring(1)) + "(){\n");
            buffer.write(" }\n\n ");
            
            for(String x : campos)
            {
                String[] campoSeparado = x.split(" / ");
                buffer.write("    public " + campoSeparado[1] + " get" + campoSeparado[0].substring(0,1).toUpperCase().concat(campoSeparado[0].substring(1)) + "() { \n" + 
                        "        return " + campoSeparado[0] + "; \n" +
                        "    } \n \n" + 
                        "    public void set" + campoSeparado[0].substring(0,1).toUpperCase().concat(campoSeparado[0].substring(1)) + "(" + campoSeparado[1] + " " + campoSeparado[0] + ") { \n" +
                        "        this." + campoSeparado[0] + " = " + campoSeparado[0] + "; \n" + 
                        "    } \n \n");   
            }
            buffer.write("}");
            buffer.close();  
            
            
    }
    
    public void criadorDAO(ArrayList<String> campos, String titulo, ArrayList<String> funcionalidades, String primaryKey) throws IOException /** cria o DAO **/
    {
        String tipo = "";
        for (String campo: campos) {
            String[] metacampos = campo.split(" / ");
            if (metacampos[0].equals(primaryKey)) {
                tipo = metacampos[1];
                break;
            }
        }
        BufferedWriter buffer;
        buffer = new BufferedWriter(new FileWriter(new File("C:\\" + titulo + "\\model\\DAO" +  titulo.substring(0,1).toUpperCase().concat(titulo.substring(1)) + ".java"))); /** cria o arquivo **/
        buffer.write("package model;\n" +
                "\n" +
                "import java.sql.Connection;\n" +
                "import java.sql.PreparedStatement;\n" +
                "import java.sql.ResultSet;\n" +
                "import java.sql.SQLException;\n \n" +
                "import model."+titulo.substring(0, 1).toUpperCase().concat(titulo.substring(1))+";\n"+
                "public class DAO" + titulo.substring(0,1).toUpperCase().concat(titulo.substring(1)) + " {\n \n" +
                "    private Connection connection; \n \n" +
                "    public DAO" + titulo.substring(0,1).toUpperCase().concat(titulo.substring(1)) + "() throws SQLException \n" +
                "    {\n" +
                "        connection = ConnectionDatabase.getConnection();\n" +
                "    }\n \n");
        for(String n : funcionalidades) /** peercorre o array de funcionalidades para ver quais deve criar **/
        {
            if(n == "Salvar") /** se ele achar "salvar" no array de funcionalidades, ele cria a funcionalidade no DAO **/
            {
                buffer.write("    public boolean salvar(" + titulo.substring(0,1).toUpperCase().concat(titulo.substring(1)) + " " + titulo.substring(0,1).toLowerCase().concat(titulo.substring(1)) + ") throws SQLException\n" +
                        "    {\n" +
                        "        int retorno = 0;\n" +
                        "        \n" +
                        "        String sql = \"INSERT into " + titulo.substring(0,1).toLowerCase().concat(titulo.substring(1)) + "("); 
                
                int contadorCampos = 0;
                for(String x : campos)
                {
                    String[] campoSeparado = x.split(" / ");
                    
                    if(contadorCampos+1 != campos.size())
                    {
                        buffer.write(campoSeparado[0]+", ");
                    }
                    else
                    {
                        buffer.write(campoSeparado[0]+") ");
                    }
                    contadorCampos++;
                }
                
                /*for (int x = 0; x < campos.size(); x++) 
                {
                    if(x+1 != campos.size())
                    {
                        buffer.write(campos.get(x)+", ");
                    }
                    else
                    {
                        buffer.write(campos.get(x)+") ");
                    }
                }*/
                
                buffer.write("VALUES(");
                for (int x = 1; x <= campos.size(); x++) 
                {
                    if(x != campos.size())
                    {
                        buffer.write("?, ");
                    }
                    else
                    {
                        buffer.write("?)\";\n\n");
                    }
                }
                buffer.write("        PreparedStatement statement = connection.prepareStatement(sql);\n" +
                            "\n");
                int contcampos = 1;
                for(String x : campos)
                {
                    String[] campoSeparado = x.split(" / ");
                    buffer.write("        statement.set" + campoSeparado[1].substring(0,1).toUpperCase().concat(campoSeparado[1].substring(1)) + "(" + contcampos + ", " + titulo.substring(0,1).toLowerCase().concat(titulo.substring(1))+ ".get" + campoSeparado[0].substring(0,1).toUpperCase().concat(campoSeparado[0].substring(1)) + "());\n");
                    contcampos++;
                }
                
                buffer.write("        return statement.executeUpdate() > 0;\n");
                
                buffer.write("    }\n\n");
            }
            
            if(n == "Remover"){
                buffer.write("    public boolean remover("+titulo.substring(0,1).toUpperCase().concat(titulo.substring(1))+" "+titulo+") throws SQLException{ \n");
                buffer.write("        int retorno = 0; \n");
                buffer.write("        String sql = \"DELETE FROM "+titulo.substring(0,1).toLowerCase().concat(titulo.substring(1))+" WHERE "+primaryKey.substring(0, 1).toLowerCase().concat(primaryKey.substring(1))+" = ? \"; \n");
                buffer.write("        PreparedStatement stmt = connection.prepareStatement(sql);\n");
                buffer.write("        stmt.set"+tipo.substring(0, 1).toUpperCase().concat(tipo.substring(1))+"(1, "+titulo.substring(0,1).toLowerCase().concat(titulo.substring(1))+".get"+primaryKey.substring(0, 1).toUpperCase().concat(primaryKey.substring(1))+"()); \n");
                buffer.write("        retorno = stmt.executeUpdate(); \n");
                buffer.write("        return retorno > 0; \n     } \n");
            }
            
            if(n == "Consultar"){
                String classe = titulo.substring(0,1).toUpperCase().concat(titulo.substring(1));
                String var = titulo.substring(0,1).toLowerCase().concat(titulo.substring(1));
                buffer.write("    public "+classe+" consultar("+classe+" "+var+") throws SQLException{\n");
                buffer.write("        "+classe+" p = null;\n");
                buffer.write("        String sql = \" SELECT * FROM "+var+" WHERE "+primaryKey+" = ? \"; \n");
                buffer.write("        PreparedStatement stmt = connection.prepareStatement(sql);\n");
                buffer.write("        stmt.set"+tipo.substring(0, 1).toUpperCase().concat(tipo.substring(1))+"(1, "+var+".get"+primaryKey.substring(0, 1).toUpperCase().concat(primaryKey.substring(1))+"());\n");
                buffer.write("        ResultSet rs = stmt.executeQuery();\n");
                buffer.write("        while(rs.next()){\n");
                buffer.write("            p = new "+classe+"();\n");
                for (String campo: campos) {
                    String[] campoSeparado = campo.split(" / ");
                    String varUpper = campoSeparado[0].substring(0, 1).toUpperCase().concat(campoSeparado[0].substring(1));
                    String varLower = campoSeparado[0].substring(0, 1).toLowerCase().concat(campoSeparado[0].substring(1));
                    String tipoUpper = campoSeparado[1].substring(0, 1).toUpperCase().concat(campoSeparado[1].substring(1));
                    buffer.write("            p.set"+varUpper+"(rs.get"+tipoUpper+"(\""+varLower+"\")); \n");
                }
                buffer.write("}\n");
                buffer.write("        return p; \n\n");
                buffer.write("    }\n");
            }
            
            if(n == "Editar"){
                String classe = titulo.substring(0,1).toUpperCase().concat(titulo.substring(1));
                String var = titulo.substring(0,1).toLowerCase().concat(titulo.substring(1));
                buffer.write("    public boolean editar("+classe+" "+var+") throws SQLException{\n");
                buffer.write("        String sql = \" UPDATE "+var+" SET ");
                String pkNome = "";
                String pkTipo = "";
                int contcampos = 1;
                
                ArrayList<String> novosCampos = new ArrayList<String>();
                for (String campo: campos) {
                    String[] campoSeparado = campo.split(" / ");
                    if (!campoSeparado[0].equals(primaryKey)) {
                        novosCampos.add(campo);
                    }else{
                        pkNome = campoSeparado[0];
                        pkTipo = campoSeparado[1];
                    }
                }
                
                for (String campo: novosCampos) {
                    String[] campoSeparado = campo.split(" / ");
                    if (contcampos == 1) {
                        buffer.write(campoSeparado[0]+" = ?");
                    }else{
                        buffer.write(", "+campoSeparado[0]+" = ?");
                    }
                    contcampos++;
                }
                
                buffer.write(" WHERE "+pkNome+" = ? \"; \n");
                buffer.write("        PreparedStatement stmt = connection.prepareStatement(sql);\n");
                contcampos = 1;
                for(String campo: novosCampos){
                    String[] campoSeparado = campo.split(" / ");
                    String varUpper = campoSeparado[0].substring(0, 1).toUpperCase().concat(campoSeparado[0].substring(1));
                    String varLower = campoSeparado[0].substring(0, 1).toLowerCase().concat(campoSeparado[0].substring(1));
                    String tipoUpper = campoSeparado[1].substring(0, 1).toUpperCase().concat(campoSeparado[1].substring(1));
                    buffer.write("        stmt.set"+tipoUpper+"("+contcampos+", "+var+".get"+varUpper+"());\n");
                    contcampos++;
                }
                buffer.write("        stmt.set"+pkTipo.substring(0, 1).toUpperCase().concat(pkTipo.substring(1))+"("+contcampos+",  "+var+".get"+pkNome.substring(0, 1).toUpperCase().concat(pkNome.substring(1))+"());\n");
                buffer.write("        int resultado = stmt.executeUpdate();\n");
                buffer.write("        return resultado > 0;\n     } \n");
                
            }
        }
        buffer.write("}\n\n");
        buffer.close();
    }
    
    
}
